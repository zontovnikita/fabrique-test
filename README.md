**ЗАПУСК ПРОЕКТА**

1. Скачать и разорхивировать проект, открыть в удобной IDE (Visual Studio Code, PyCharm)
2. Подключение к базе данных MySQL (settings.py) // https://docs.djangoproject.com/en/4.0/ref/databases/
3. Провести миграции: python manage.py makemigrations, python manage.py migrate
4. Запуск проекта python manage.py runserver
5. Запуск процессов Celery (celery -A appsender worker -l info -P eventlet, celery -A appsender beat -l info)
6. Проект запущен и работает!

> P.S. venv внутри репозитория.

> P.S.S. Для работы с локальной БД, рекомендую WAMPSERVER, OPENSERVER

> P.S.S.S Celery не работает на Windows, поэтому нужно иметь WSL для запуска Redis

**ДОКУМЕНТАЦИЯ ПО API**
1. Запустить проект (описано выше)
2. Перейти по ссылке http://localhost:your_port/docs/


**ДОПОЛНИТЕЛЬНЫЕ ЗАДАНИЯ**
1. Посуточная отправка email
2. Тестирование (основные методы)
3. drf_yasg
4. Логирование
