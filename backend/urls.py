from django.urls import path
from backend.views import ClientAPIDetail, ClientAPICreate, MailingAPICreate, MailingAPIDetail, general_statistics, \
    check_mailings, detailed_statistics, sendmail_statistics

urlpatterns = [
    path('client/', ClientAPICreate.as_view()),
    path('client/<int:pk>/', ClientAPIDetail.as_view()),
    path('mailing/', MailingAPICreate.as_view()),
    path('mailing/<int:pk>/', MailingAPIDetail.as_view()),
    path('statistics/', general_statistics),
    path('statistics/detailed/<int:pk>', detailed_statistics),
    path('send/', check_mailings),
    path('send/email/', sendmail_statistics)
]
