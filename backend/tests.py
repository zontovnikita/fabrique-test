from django.test import TestCase
from rest_framework import status
from rest_framework.test import APITestCase


# Тест на добавление клиента
class ClientTestCase(APITestCase):
    def test_create_client(self):
        data = {"phone_number": "79851607080", "code": 903, "time_zone": "Moscow"}
        response = self.client.post('http://localhost:8000/api/v1/client/', data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


class StatisticsTestCase(APITestCase):
    def test_get_statistics(self):
        response = self.client.get('http://localhost:8000/api/v1/statistics/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class CheckDateTestCase(APITestCase):
    def test_check_date(self):
        response = self.client.get('http://localhost:8000/api/v1/send/')
        self.assertEqual(response.data['status'], 'OK')
