from django.core.validators import RegexValidator
from django.db import models


# Create your models here.
class Mailing(models.Model):
    date_start = models.DateTimeField(verbose_name='Дата и время запуска')
    date_end = models.DateTimeField(verbose_name='Дата и время окончания')
    text = models.TextField(verbose_name='Текст сообщения')
    code = models.IntegerField(verbose_name='Код мобильного оператора')
    status = models.BooleanField(verbose_name='Статус', auto_created=True, default=False)

    def __str__(self):
        return f'{self.id}'

    class Meta:
        db_table = 'mailing'
        verbose_name = 'Рассылка'
        verbose_name_plural = 'Рассылки'


class Client(models.Model):
    phone_regex = RegexValidator(regex=r'^(7)\d{10}', message='Неверный формат для номера телефона')
    phone_number = models.CharField(max_length=11, verbose_name='Номер телефона', validators=[phone_regex], unique=True)
    code = models.IntegerField(verbose_name='Код мобильного оператора')
    time_zone = models.CharField(max_length=25, verbose_name='Часовой пояс')

    def __str__(self):
        return f'{self.phone_number}'

    class Meta:
        db_table = 'client'
        verbose_name = 'Клиент'
        verbose_name_plural = 'Клиенты'


class Message(models.Model):
    date_go = models.DateTimeField(auto_now=True, verbose_name='Дата и время отправки')
    status = models.CharField(max_length=15, verbose_name='Статус сообщения')
    mailing = models.ForeignKey(to=Mailing, verbose_name='ID рассылки', on_delete=models.CASCADE)
    client = models.ForeignKey(to=Client, verbose_name='ID клиента', on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.id}'

    class Meta:
        db_table = 'message'
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'
