from django.utils import timezone
from django.conf import settings
from rest_framework import generics
from rest_framework.response import Response
from rest_framework.decorators import api_view
from twilio.rest import Client as TwilioClient
from multiprocessing import Process
from .models import Client, Message, Mailing
from django.core.mail import EmailMessage
from .serializers import ClientSerializer, MailingSerializer
import logging


# Create your views here.

class ClientAPICreate(generics.CreateAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class ClientAPIDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class MailingAPICreate(generics.CreateAPIView):
    queryset = Mailing.objects.all()
    serializer_class = MailingSerializer


class MailingAPIDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Mailing.objects.all()
    serializer_class = MailingSerializer


@api_view(['GET'])
def general_statistics(request):
    if request.method == 'GET':
        mailings = Mailing.objects.filter(status=True).values()
        m = list(mailings)
        for i in m:
            accept = Message.objects.filter(mailing_id=i['id'], status='queued').values()
            failed = Message.objects.filter(mailing_id=i['id'], status='error').values()
            i['score_messages'] = len(accept) + len(failed)
            i['score_accept'] = len(accept)
            i['score_failed'] = len(failed)
            i['messages'] = {'accept': accept, 'failed': failed}
        return Response(m)


@api_view(['GET'])
def detailed_statistics(request, pk):
    if request.method == 'GET':
        m = Mailing.objects.all().filter(id=pk).values().first()
        accept = Message.objects.filter(mailing_id=m['id'], status='queued').values()
        failed = Message.objects.filter(mailing_id=m['id'], status='error').values()
        m['score_messages'] = len(accept) + len(failed)
        m['score_accept'] = len(accept)
        m['score_failed'] = len(failed)
        m['messages'] = {'accept': accept, 'failed': failed}
        return Response(m)


@api_view(['GET'])
def check_mailings(request):
    if request.method == 'GET':
        logging.log(1, 'Hello')
        print('Началась проверка даты')
        mailings = Mailing.objects.all()
        now = timezone.now()
        for i in mailings:
            if i.date_start < now < i.date_end and i.status is False:
                p = Process(target=send_messages(i.code, i.id))
                p.start()
                mailing = Mailing.objects.get(id=i.id)
                mailing.status = True
                mailing.save()
        return Response({'status': 'OK', 'code': 200})


def send_messages(code, pk_mailing):
    clients = Client.objects.filter(code=code).all()
    twilio = TwilioClient(settings.TWILIO_ACCOUNT_SID, settings.TWILIO_ACCOUNT_TOKEN)
    for i in clients:
        try:
            message = twilio.messages.create(body=f'Hello,It`s Fabrique Studio!',
                                             from_=settings.TWILIO_PHONE_NUMBER, to=f'+{i.phone_number}')
            Message.objects.create(status=message.status, mailing_id=pk_mailing, client_id=i.id)
        except Exception:
            Message.objects.create(status='error', mailing_id=pk_mailing, client_id=i.id)


@api_view(['GET'])
def sendmail_statistics():
    try:
        mail = EmailMessage('Статистика за месяц', create_statistics(), 'from@example.com', 'to@example.com')
        mail.send()
        return Response({'status': 'SEND'})
    except Exception:
        return Response({'status': 'ERROR'})


def create_statistics():
    mailings = len(Mailing.objects.filter(status=True, date_end__day=timezone.now().today()).all())
    return f'Сегодня обработано рассылок: {mailings}'
