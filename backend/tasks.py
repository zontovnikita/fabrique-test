from __future__ import absolute_import, unicode_literals
import requests
from celery import shared_task


@shared_task
def check():
    print('Запустился воркер!')
    requests.get('http://127.0.0.1:8000/api/v1/send/')


@shared_task
def send():
    print('Отправка результатов за месяц')
    requests.get('http://127.0.0.1:8000/api/v1/send/email/')
