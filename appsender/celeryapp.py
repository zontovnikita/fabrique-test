from __future__ import absolute_import
import os
from celery import Celery
from celery.schedules import crontab

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'appsender.settings')
app = Celery("appsender", broker="redis://127.0.0.1:6379/0", backend="redis://127.0.0.1:6379/0",
             include=['backend.tasks'])

app.config_from_object('django.conf:settings', namespace='CELERY')

app.conf.beat_schedule = {
    "run-every-minute": {
        "task": "backend.tasks.check",
        "schedule": 60
    },
    "send-every-month": {
        "task": "backend.tasks.send",
        "schedule": crontab(hour=12, minute=0)
    }
}

app.autodiscover_tasks()
